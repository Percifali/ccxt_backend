require("dotenv").config();
const express = require("express");
const http = require("http");
const fs = require("fs");

const MONGO_USERNAME = 'ccxtadmin';
const MONGO_PASSWORD = 'ccxtadmin';
const MONGO_HOSTNAME = '127.0.0.1';
const MONGO_PORT = '27017';
const MONGO_DB = 'ccxt';
//const url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`;
const url = `mongodb://host.docker.internal:27017/ccxt`;
//const options = {
//  key: fs.readFileSync('example.key'),
//  cert: fs.readFileSync('example.crt')
//};

const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const socket = require("./socket");

console.log(url);

mongoose
  .connect(url)
  .then(() => console.log("connected to database successfully"))
  .catch((err) => console.log(err));

const router = require("./router");

const port = process.env.PORT;
const app = express();
const server = http.createServer(app);

socket.initIO(server);

app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(
  bodyParser.urlencoded({
    // to support URL-encoded bodies
    extended: true,
  })
);
app.use(cors());
app.use("/api", router);

server.listen(port, "0.0.0.0", () =>
  // eslint-disable-next-line no-console
  console.log(`server is running`, port)
);
