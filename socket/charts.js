const ccxtpro = require ('ccxt.pro');
const { Server } = require('socket.io');

const { sleep } = require('./common');

exports.initIO = (httpServer) => {
    let io = new Server( httpServer, { path: '/charts/', cors: {
        origin: process.env.FRONT_URL,
      },});
    initRoutes(io);
    return io;
}

let exchange = new ccxtpro.binance ({
    newUpdates: true,
    options: {
        OHLCVLimit: 1
    }
});
(async () => {
    await exchange.loadMarkets ();
})();

function initRoutes(io) {
    io.on('connection', async (socket) => {
        console.log("connected charts", socket.id)
        await mainCycle(socket);
    })
}

async function mainCycle(socket) {
    var connected = true;

    var userData = {
      currentSymbol: 'ETH/USDT',
      currentResolution: '1h',
    };

    socket.on('new_symbol', (newSymbol) => {
        userData.currentSymbol = newSymbol;
    });
    socket.on('new_resolution', (newResolution) => {
        if (newResolution !== userData.currentResolution) {
            userData.currentResolution = newResolution;
        }
    });

    socket.on("disconnect", (reason) => {
        console.log("disconnected tradelist", socket.id)
        connected = false;
    })

    while (connected) {
        try {
            let candles = await exchange.watchOHLCV(userData.currentSymbol, userData.currentResolution)
            socket.emit('charts', candles)
            await sleep(300);
        } catch (e) {
            console.log("wathcOHLCV Exception: ", e);
        }
    }
}