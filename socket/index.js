const orderbook = require("./orderbook");
const tradelist = require("./tradelist");
const charts = require("./charts");

// module.exports = (io, socket) => {
//     const createOrder = (payload) => {
//       // ...
//     }

//     const readOrder = (orderId, callback) => {
//       // ...
//     }

//     socket.on("order:create", createOrder);
//     socket.on("order:read", readOrder);
// }

// {
//     onConnection: async (socket) => {
//         exchange = new ccxtpro.binance({
//             newUpdates: true,
//         });
//         await exchange.loadMarkets();
//     }
// }

let orderbookServer, tradelistServer, chartsServer;

exports.initIO = (httpServer) => {
  orderbookServer = orderbook.initIO(httpServer);
  tradelistServer = tradelist.initIO(httpServer);
  chartsServer = charts.initIO(httpServer);
};
