const ccxtpro = require("ccxt.pro");
const { Server } = require("socket.io");


const { aggregateOrderBook } = require("../utils");

const ordersLimit = 5000; 

const possibleSortByParameters = ['price', 'amount', 'total'];
const sortByCallbacks = {
    'price': (a, b) => b[0] - a[0],
    'amount': (a, b) => b[1] - a[1],
    'total': (a, b) => b[0] * b[1] - a[0] * a[1]
};

var map = new Map();

let exchange = new ccxtpro.binance ({
    newUpdates: true,
    options: {
        ordersLimit: ordersLimit
    }
});

(async () => {
    await exchange.loadMarkets ();
})();

exports.initIO = (httpServer) => {
  let io = new Server(httpServer, {
    path: "/orderbook/",
    cors: {
      origin: process.env.FRONT_URL,
    },
  });
  initRoutes(io);
  return io;
};

function initRoutes(io) {
  io.on("connection", async (socket) => {
    console.log("connected orders", socket.id);
    await mainCycle(socket);
  }); 
}

async function mainCycle(socket) {
    var connected = true;

    var userData = {
      currentSymbol: 'ETH/USDT',
      currentPrecision: 0.1,
      currentSortBy: 'total',
      aggregatedOrderBook: null,
      firstTime: true,
    };

    if (socket.handshake["query"]["initial_symbol"]) {
      userData.currentSymbol = socket.handshake["query"]["initial_symbol"];
    }
    if (socket.handshake["query"]["initial_precision"]) {
      userData.currentPrecision = socket.handshake["query"]["initial_precision"];
    }
    if (socket.handshake["query"]["initial_sort_by"] && possibleSortByParameters.includes(socket.handshake["query"]["initial_sort_by"])) {
      userData.currentSortBy = socket.handshake["query"]["initial_sort_by"];
    }

    await exchange.loadMarkets();

    socket.on("new_symbol", (newSymbol) => {
      userData.firstTime = true;
      userData.currentSymbol = newSymbol;
    });
    socket.on("new_precision", (newPrecision) => {
      userData.firstTime = true;
      userData.currentPrecision = newPrecision;
    });
    socket.on("new_sort_by", (newSortBy) => {
      userData.firstTime = true;
      userData.currentSortBy = newSortBy;
    });

    socket.on("disconnect", (reason) => {
      console.log("disconnected orders", socket.id)
      connected = false;
    })

    while (connected) {
        try {
            const newOrderBook = await exchange.watchOrderBook(userData.currentSymbol);
            let newAggregatedOrderBook = aggregateOrderBook(newOrderBook, userData.currentPrecision);
            newAggregatedOrderBook.asks.sort(sortByCallbacks[userData.currentSortBy]);
            if (userData.currentSortBy === "total") newAggregatedOrderBook.asks.reverse();
            newAggregatedOrderBook.bids.sort(sortByCallbacks[userData.currentSortBy]);
            const orderBookToSend = mergeAggregatedOrderBook(userData.aggregatedOrderBook, newAggregatedOrderBook, userData.firstTime);
            socket.emit('orderbook', orderBookToSend);

            userData.aggregatedOrderBook = newAggregatedOrderBook;
            if (userData.firstTime) userData.firstTime = false;
        } catch (e) {
            console.log("watchOrderBook Exception: ", e);
        }
    }
}

function filterOrderBookSide(aggregatedOrderBook, orderBook, sideName, first = false) {
    return orderBook[sideName].map((ask, i) => {
        return {
            new: first || !ask.every((value, j) => {
              if (!aggregatedOrderBook[sideName][i]) return false;
              return value === aggregatedOrderBook[sideName][i][j];
            }),
            price: orderBook[sideName][i][0].toFixed(2), 
            amount: orderBook[sideName][i][1].toFixed(2),
            total: (orderBook[sideName][i][0] * orderBook[sideName][i][1]).toFixed(2)
        };
    });
}

function mergeAggregatedOrderBook(aggregatedOrderBook, newAggregatedOrderBook, first) {
    const filteredAsks = filterOrderBookSide(aggregatedOrderBook, newAggregatedOrderBook, 'asks', first);
    const filteredBids = filterOrderBookSide(aggregatedOrderBook, newAggregatedOrderBook, 'bids', first);

    let max = 0;
    Object.values(filteredAsks).concat(Object.values(filteredBids)).map((order) => {
      if (parseFloat(order.total) > max) max = parseFloat(order.total);
    })

    return {
        'asks': filteredAsks,
        'bids': filteredBids,
        'max': max,
        'timestamp': newAggregatedOrderBook['timestamp'],
        'datetime': newAggregatedOrderBook['datetime'],
        'nonce': newAggregatedOrderBook['nonce']
    };
}

function changeSymbol(newSymbol, userId) {
  const currentData = map.get(userId);
  if (newSymbol && currentData) {
    map.set(userId, {...currentData, firstTime: true, currentSymbol: newSymbol})

  }
}

function changePrecision(newPrecision, userId) {
  const currentData = map.get(userId);
  if (newPrecision && currentData) {
    map.set(userId, {...currentData, firstTime: true, currentPrecision: newPrecision})

  }
}

function changeSortBy(newSortBy, userId, data) {
  const currentData = map.get(userId);
  if (newSortBy && possibleSortByParameters.includes(newSortBy) && currentData) {
    map.set(userId, {...currentData, firstTime: true, currentSortBy: newSortBy}) 
  }
}