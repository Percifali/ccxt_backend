const ccxtpro = require ('ccxt.pro');
const { Server } = require('socket.io');

const { sleep } = require('./common');

const tradesLimit = 100;

exports.initIO = (httpServer) => {
    let io = new Server( httpServer, { path: '/tradelist/', cors: {
        origin: process.env.FRONT_URL,
      },});
    initRoutes(io);
    return io;
}

let exchange = new ccxtpro.binance ({
    newUpdates: true
});
(async () => {
    await exchange.loadMarkets ();
})();

function initRoutes(io) {
    io.on('connection', async (socket) => {
        console.log("connected tradelist", socket.id)
        await mainCycle(socket);
    })
}

function filterTrades(trades, currentRange) {
    return trades.filter(trade => {
        return (!currentRange[0] || trade.cost >= currentRange[0])
            && (!currentRange[1] || trade.cost <= currentRange[1]);
    })
}

async function mainCycle(socket) {
    var connected = true;

    var userData = {
      currentSymbol: 'ETH/USDT',
      currentRange: [0,0],
      firstTime: true,
      since: undefined,
    };

    socket.on('new_symbol', (newSymbol) => {
        userData.since = undefined
        userData.firstTime = true;
        userData.currentSymbol = newSymbol;
    });
    socket.on('new_range', (newRange) => {
        console.log(newRange)
        if (newRange !== userData.currentRange) {
            userData.firstTime = true;
            userData.currentRange = newRange;
        }
    });

    socket.on("disconnect", (reason) => {
        console.log("disconnected tradelist", socket.id)
        connected = false;
    })

    while (connected) {
        try {
            let trades;
            if (userData.firstTime) {
                const limit = (userData.currentRange[0] || userData.currentRange[1]) ? 5000 : tradesLimit;
                trades = await exchange.fetchTrades(userData.currentSymbol, userData.since, limit);
                userData.firstTime = false;
                trades = filterTrades(trades, userData.currentRange);
                socket.emit('tradelist', trades.reverse().slice(0, 100));
            } else {
                trades = await exchange.watchTrades(userData.currentSymbol, userData.since, tradesLimit);
                trades = filterTrades(trades, userData.currentRange);
                if (trades.length > 0) {
                    userData.since = trades[0]['timestamp'];
                    socket.emit('tradelist', trades.reverse());
                }
            }

            await sleep(300);
        } catch (e) {
            console.log("watchTrades Exception: ", e);
        }
    }
}