const ccxt = require("ccxt");

const aggregateOrderBookSide = function (orderbookSide, precision = undefined) {
  const result = []
  const amounts = {}
  for (let i = 0; i < orderbookSide.length; i++) {
      const ask = orderbookSide[i]
      let price = ask[0]
      if (precision !== undefined) {
          price = ccxt.decimalToPrecision (price, ccxt.ROUND, precision, ccxt.TICK_SIZE)
      }
      amounts[price] = (amounts[price] || 0) + ask[1]
  }
  Object.keys (amounts).forEach (price => {
      result.push ([
          parseFloat (price),
          amounts[price]
      ])
  })
  return result
}

const aggregateOrderBook = function (orderbook, precision = undefined) {
  let asks = aggregateOrderBookSide(orderbook['asks'], precision)
  let bids = aggregateOrderBookSide(orderbook['bids'], precision)
  return {
      'asks': ccxt.sortBy (asks, 0).slice(0, 12),
      'bids': ccxt.sortBy (bids, 0, true).slice(0, 12),
      'timestamp': orderbook['timestamp'],
      'datetime': orderbook['datetime'],
      'nonce': orderbook['nonce'],
  };
}

module.exports = {
  aggregateOrderBook,
};
