const { Schema, model } = require("mongoose");

const watchListSchema = new Schema(
  {
    userId: { type: String },
    watchList: [Object],
  },
  {
    timestamps: {
      createdAt: "created_at",
    },
  }
);
watchListSchema.index({ userId: 1 });
const WatchList = model("WatchList", watchListSchema);

module.exports = WatchList;
