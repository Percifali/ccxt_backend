const { Schema, model } = require("mongoose");

const ViewConfigSchema = new Schema(
  {
    userId: { type: String },
    viewConfig: Object,
  },
  {
    timestamps: {
      createdAt: "created_at",
    },
  }
);
ViewConfigSchema.index({ userId: 1 });
const ViewConfig = model("ViewConfig", ViewConfigSchema);

module.exports = ViewConfig;
