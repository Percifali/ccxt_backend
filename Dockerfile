FROM node
RUN apt-get install git openssh-client
WORKDIR /usr/src/app
COPY package*.json ./
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan github.com >> ~/.ssh/known_hosts
RUN --mount=type=ssh,id=github npm install
COPY . .
EXPOSE 5000
CMD ["node", "index.js"] 