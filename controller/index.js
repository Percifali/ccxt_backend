const ccxt = require("ccxt");
const ViewConfig = require("../model/ViewConfig");
const WatchList = require("../model/WatchList");

const { aggregateOrderBook } = require("../utils");

const exchange = new ccxt.binance({
  enableRateLimit: true,
});

const assets = [
  "btc",
  "eth",
  "dot",
  "near",
  "sol",
  "avax",
  "ftm",
  "matic",
  "atom",
  "oasis",
];

const resolutions = [
  "1m",
  "5m",
  "10m",
  "30m",
  "1h",
  "3h",
  "1d",
  "3d",
  "7d",
  "21d",
  "1M",
];
let markets = null;

exchange.loadMarkets().then((data) => {
  markets = data;
});

const getConfig = (req, res) => {
  res.status(200).json({
    supports_search: true,
    supports_group_request: false,
    supports_marks: false,
    supports_timescale_marks: false,
    supports_time: true,
    exchanges: [
      {
        value: "",
        name: "Binance",
        desc: "Binance",
      },
    ],
    symbols_types: [
      {
        name: "Crypto",
        value: "crypto",
      },
    ],
    supported_resolutions: resolutions,
  });
};

const getSymbols = async (req, res) => {
  try {
    const { symbol } = req.query;
    let _markets = markets;
    if (!_markets) {
      _markets = await exchange.loadMarkets();
    }
    const market = _markets[symbol];
    res.status(200).json({
      type: "crypto",
      description: market.symbol,
      symbol: market.symbol,
      name: market.symbol,
      ticker: market.symbol,
      exchange: "binance",
      has_intraday: true,
      has_dwm: true,
      has_empty_bars: true,
      visible_plots_set: "ohlc",
      supported_resolutions: resolutions,
      ...market,
    });
  } catch (err) {
    res.status(500).json({
      s: "error",
      errmsg: err.message,
    });
  }
};

const getTime = async (req, res) => {
  try {
    const time = await exchange.fetchTime();
    res.status(200).json(String(Math.floor(time / 1000)));
  } catch (err) {
    res.status(500).json({
      s: "error",
      errmsg: err.message,
    });
  }
};

function isNumeric(str) {
  if (typeof str != "string") return false; // we only process strings!
  return (
    !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str))
  ); // ...and ensure strings of whitespace fail
}

const getBar = async (req, res) => {
  try {
    const { symbol, resolution, from, to, countBack } = req.query;

    // const timeframes = exchange.timeframes;
    // if (resolution === "1D") {
    //   _resolution = "1d";
    // } else if (isNumeric(resolution)) {
    //   if (Number(resolution) >= 60) {
    //     _resolution = Math.floor(Number(resolution) / 60) + "h";
    //     if (!timeframes[_resolution]) {
    //       let tmp = 1;
    //       Object.keys(timeframes).forEach((item) => {
    //         if (item[item.length - 1] === "h") {
    //           const num = item.slice(0, item.length - 1);
    //           const resNum = _resolution.slice(0, _resolution.length - 1);
    //           if (Number(num) < Number(resNum)) {
    //             tmp = item;
    //           }
    //         }
    //       });
    //       _resolution = tmp;
    //     }
    //   } else {
    //     _resolution = Number(resolution) + "m";
    //     if (!timeframes[_resolution]) {
    //       let tmp = 1;
    //       Object.keys(timeframes).forEach((item) => {
    //         if (item[item.length - 1] === "m") {
    //           const num = item.slice(0, item.length - 1);
    //           const resNum = _resolution.slice(0, _resolution.length - 1);
    //           if (Number(num) < Number(resNum)) {
    //             tmp = item;
    //           }
    //         }
    //       });
    //       _resolution = tmp;
    //     }
    //   }
    // } else {
    //   _resolution = "1d";
    // }

    const data = await exchange.fetchOHLCV(
      symbol,
      resolution,
      countBack ? undefined : from * 1000,
      countBack,
      { endTime: to * 1000 }
    );
    const t = [];
    const o = [];
    const h = [];
    const l = [];
    const c = [];
    const v = [];
    const s = "ok";

    data.forEach((item) => {
      t.push(item[0] / 1000);
      o.push(item[1]);
      h.push(item[2]);
      l.push(item[3]);
      c.push(item[4]);
      v.push(item[5]);
    });
    res.status(200).json({
      t,
      o,
      h,
      l,
      c,
      v,
      s,
    });
  } catch (err) {
    res.status(200).json({
      t: [],
      o: [],
      h: [],
      l: [],
      c: [],
      v: [],
      s: "ok",
    });
  }
};

const getSearchSymbol = async (req, res) => {
  try {
    const { query, limit } = req.query;
    const markets = await exchange.loadMarkets();
    const marketArr = Object.entries(markets).map(([symbol, market]) => {
      return market;
    });
    const filteredMarkets = marketArr.filter(({ base, quote }) => {
      if (assets.includes(base.toLowerCase()) && quote === "USDT") {
        if (!query || query === "") return true;
        const str = base + quote;
        const str2 = base + "/" + quote;
        if (
          str.toLowerCase().includes(query.toLowerCase()) ||
          str2.toLowerCase().includes(query.toLowerCase())
        )
          return true;
        else return false;
      } else return false;
    });
    const symbols = filteredMarkets.map((market) => ({
      type: "crypto",
      description: market.symbol,
      symbol: market.symbol,
      name: market.symbol,
      ticker: market.symbol,
      exchange: "binance",
      has_intraday: true,
      supported_resolutions: resolutions,
    }));
    res.status(200).json(symbols);
  } catch (err) {
    console.log(err);
    res.status(500).json({
      s: "error",
      errmsg: err.message,
    });
  }
};

const getMarkets = async (req, res) => {
  try {
    const markets = await exchange.loadMarkets();
    const marketArr = Object.entries(markets).map(([symbol, market]) => {
      return market;
    });
    const filteredMarkets = marketArr.filter(({ base, quote }) => {
      if (assets.includes("eth") || assets.includes(quote.toLowerCase()))
        return true;
      else return false;
    });
    res.status(200).json({ result: true, data: filteredMarkets });
  } catch {
    res.status(500).json({ error: "Server error" });
  }
};

const getWatchList = async (req, res) => {
  try {
    const { userId } = req.params;
    const watchList = await WatchList.findOne({ userId });
    res.status(200).json({ result: true, data: watchList });
  } catch (err) {
    res.status(500).json({ error: "Server error" });
  }
};

const saveWatchList = async (req, res) => {
  try {
    const { userId } = req.params;
    const { watchList } = req.body;
    await WatchList.updateOne({ userId }, { watchList }, { upsert: true });
    res.status(200).json({ result: true, data: true });
  } catch (err) {
    res.status(500).json({ error: "Server error" });
  }
};

const removeWatchList = async (req, res) => {
  try {
    const { userId } = req.params;
    const { id } = req.body;
    await WatchList.updateOne({ userId }, { $pull: { watchList: { symbol: id } } });
    res.status(200).json({ result: true, data: true });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Server error" });
  }
};

const getViewConfig = async (req, res) => {
  try {
    const { userId } = req.params;
    const viewConfig = await ViewConfig.findOne({ userId });
    res.status(200).json({ result: true, data: viewConfig });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Server error" });
  }
};

const saveViewConfig = async (req, res) => {
  try {
    const { userId } = req.params;
    const { viewConfig } = req.body;
    await ViewConfig.updateOne({ userId }, { viewConfig }, { upsert: true });
    res.status(200).json({ result: true, data: true });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Server error" });
  }
};

const searchAssets = async (req, res) => {
  try {
    const { searchStr } = req.params;
    const markets = await exchange.loadMarkets();
    const marketArr = Object.entries(markets).map(([symbol, market]) => {
      return market;
    });
    const filteredMarkets = marketArr.filter(
      ({ symbol, base, baseId, quote, quoteId, type }) => {
        if (
          type === "spot" &&
          (assets.includes(base.toLowerCase()) ||
            assets.includes(baseId.toLowerCase())) &&
          (quoteId.toLowerCase() === "usdt" || quote.toLowerCase() === "usdt")
        ) {
          if (searchStr === "all") return true;
          else if (symbol.toLowerCase()?.includes(searchStr?.toLowerCase()))
            return true;
          return false;
        } else return false;
      }
    );
    res.status(200).json({ result: true, data: filteredMarkets });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ error: "Server error" });
  }
};

async function parseTicker(tickerData) {
  // const trades = await exchange.fetchTrades(tickerData.symbol, {
  //   startTime: tickerData.info.openTime,
  //   endTime: tickerData.info.closeTime
  // });
  // const bids = trades.filter(trade => trade.side === "buy").map(trade => trade.amount).reduce((a, b) => a + b, 0);
  // const asks = trades.filter(trade => trade.side === "sell").map(trade => trade.amount).reduce((a, b) => a + b, 0);
  // console.log(tickerData, bids, asks);

  return {
    symbol: tickerData.symbol,
    price: tickerData.last,
    change: tickerData.change,
    percentage: tickerData.percentage,
    volume: tickerData.quoteVolume,
    delta: 'https://vsekidki.ru/uploads/posts/2016-05/1462453744_t3wjn04u2ho.jpg' // (bids - asks) / tickerData.baseVolume * 100
  };
}

const getPriceTickers = async (req, res) => {
  try {
    const { assetArr } = req.body;
  
    const tickers = await exchange.fetchTickers(assetArr);
    res.status(200).json({
      result: true,
      data: Object.assign(
        {},
        ...await Promise.all(Object.keys(tickers)
          .map(async (tickerName) => {
            return {[tickerName]: await parseTicker(tickers[tickerName])}
          }))
        )
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Server error" });
  }

};

const getOrderBook = async (req, res) => {
  try {
    const { symbol, precision } = req.body;
    const limit = precision < 1 ? 100 : precision * 250;
    const orderBooks = await exchange.fetchL2OrderBook(symbol, limit);
    const precisionOrderBooks = aggregateOrderBook(orderBooks, precision);
    res.status(200).json({ result: true, data: precisionOrderBooks });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ error: "Server error" });
  }
};

const getTrades = async (req, res) => {
  try {
    const { symbol, since, limit } = req.body;
    const sinceTime = exchange.milliseconds() - (since ?? 1) * 86400000;
    const trades = await exchange.fetchTrades(symbol, sinceTime, limit ?? 100);
    res.status(200).json({ result: true, data: trades });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ error: "Server error" });
  }
};

const getCurrent = async (req, res) => {
  try {
    const { symbol } = req.query;

    const market = await exchange.fetchTicker(symbol);

    res.status(200).json(market);
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Server error" });
  }
};

module.exports = {
  getConfig,
  getSymbols,
  getTime,
  getBar,
  getSearchSymbol,
  getMarkets,
  searchAssets,
  getPriceTickers,
  getOrderBook,
  getWatchList,
  saveWatchList,
  removeWatchList,
  getViewConfig,
  saveViewConfig,
  getTrades,
  getCurrent,
};
