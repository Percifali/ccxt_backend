const express = require("express");
const {
  getWatchList,
  getMarkets,
  searchAssets,
  getPriceTickers,
  getOrderBook,
  saveWatchList,
  removeWatchList,
  getViewConfig,
  saveViewConfig,
  getTrades,
  getConfig,
  getSymbols,
  getTime,
  getBar,
  getSearchSymbol,
  getCurrent,
} = require("./controller");
const router = express.Router();

router.get("/config", getConfig);

router.get("/symbols", getSymbols);

router.get("/time", getTime);

router.get("/history", getBar);

router.get("/search", getSearchSymbol);

router.get("/markets", getMarkets);

router.get("/check", (req, res) => {
  res.status(200).json({ working: "yeah, workign" });
});

router.get("/watchList/:userId", getWatchList);
router.post("/watchList/:userId", saveWatchList);

router.get("/viewConfig/:userId", getViewConfig);
router.post("/viewConfig/:userId", saveViewConfig);

router.post("/watchList/:userId/remove", removeWatchList);

router.get("/assets/:searchStr", searchAssets);

router.post("/price/tickers", getPriceTickers);

router.post("/orderBook", getOrderBook);
router.post("/trades", getTrades);

router.post("/get", getCurrent);

module.exports = router;
