const ccxtpro = require("ccxt.pro");
const { Server } = require("socket.io");

const io = new Server(8000, {
  cors: {
    origin: "http://localhost:3000",
  },
});

io.on("connection", async (socket) => {
  const exchange = new ccxtpro.binance({
    newUpdates: true,
  });

  await exchange.loadMarkets();
  while (true) {
    try {
      const trades = await exchange.watchTrades("ETH/USDT");
      for (let i = 0; i < trades.length; i++) {
        const trade = trades[i];
        socket.emit("trades", trade, (response) => {
          console.log(response);
        });
        //console.log (exchange.iso8601 (exchange.milliseconds ()), exchange.id, trade['symbol'], trade['id'], trade['datetime'], trade['price'], trade['amount'])
      }
    } catch (e) {
      console.log("1", e);
    }
  }
});

// console.log ('CCXT Pro Version:', ccxtpro.version);

// async function watchExchange (exchangeId, symbol) {

//     const exchange = new ccxtpro[exchangeId] ({
//         newUpdates: true,
//     })

//     await exchange.loadMarkets ();

//     // exchange.verbose = true // uncomment for debugging purposes if necessary

// while (true) {
//     try {
//         const trades = await exchange.watchTrades (symbol)
//         for (let i = 0; i < trades.length; i++) {
//             const trade = trades[i]
//             console.log (exchange.iso8601 (exchange.milliseconds ()), exchange.id, trade['symbol'], trade['id'], trade['datetime'], trade['price'], trade['amount'])
//         }
//     } catch (e) {
//         console.log (symbol, e)
//     }
// }
// }

// async function main () {

//     const streams = {
//         'binance': 'BTC/USDT',
//     };

//     const values = Object.entries (streams)
//     const promises = values.map (([ exchangeId, symbol ]) => watchExchange (exchangeId, symbol))
//     await Promise.all (promises)
// }

// main ()
